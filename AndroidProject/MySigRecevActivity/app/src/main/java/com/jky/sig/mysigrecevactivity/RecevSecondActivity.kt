package com.jky.sig.mysigrecevactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class RecevSecondActivity : AppCompatActivity() {
    var textview :TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recev_second)

        toastShow("onCreate")
        var msg = this.intent.getStringExtra("KEY")
        Log.i("jky","onCreate:: $msg")

        var textview :TextView = findViewById(R.id.txtvGetMsg)
        textview.text =msg

        var btnClose : Button? =findViewById(R.id.btnClose)
        btnClose?.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                //TODO("Not yet implemented")

                setResult(2001)
                finish();
            }
        })

    }

    override fun onResume() {
        super.onResume()
        toastShow("onResume")
        var msg = this.intent.getStringExtra("KEY")
        Log.i("jky","onResume:: $msg")
        setResult(2001)
        //finish();
        //Log.i("jky","onResume:: $msg")
        textview?.text =msg

    }

    fun toastShow(msg : String){
        Toast.makeText(this@RecevSecondActivity, msg,Toast.LENGTH_SHORT).show()
    }
}