package com.jky.sig.mysignalapp

import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    var mEdtSendMsg : EditText? =null
    var mBtnSendSecondActivity : Button? = null

    var mBtnSendRecevAppSecondActivity : Button? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.mEdtSendMsg = findViewById(R.id.edtMsg)
        this.mBtnSendSecondActivity = findViewById(R.id.btnSendSecondActivity)
        this.mBtnSendRecevAppSecondActivity = findViewById(R.id.btnSendRecevAppSecondActivity)

        this.mBtnSendSecondActivity?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //TODO("Not yet implemented")
                Log.i("jky", mEdtSendMsg?.text.toString())
                val intent = Intent(this@MainActivity, SecondActivity::class.java).apply {
                    putExtra("KEY", mEdtSendMsg?.text.toString())
                }
                startActivity(intent)

            }
        })

        this.mBtnSendRecevAppSecondActivity?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //TODO("Not yet implemented")

//                val intent = packageManager.getLaunchIntentForPackage("com.example.testapp")
//                intent!!.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
                var compName : ComponentName = ComponentName("com.jky.sig.mysigrecevactivity","com.jky.sig.mysigrecevactivity.RecevSecondActivity")
                var intent : Intent = Intent(Intent.ACTION_MAIN)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addCategory(Intent.CATEGORY_LAUNCHER)
                intent.putExtra("KEY", mEdtSendMsg?.text.toString())
                intent.setComponent(compName);
                startActivity(intent)
                startActivityForResult(intent,101)

            }

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Toast.makeText(this@MainActivity,"requestCode: $requestCode, resultCode: $resultCode",Toast.LENGTH_SHORT).show()
    }
}