package com.jky.sig.mysignalapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class SecondActivity : AppCompatActivity() {

    var mTxtvGetMsg : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val message = this.getIntent().getStringExtra("KEY")

        // Capture the layout's TextView and set the string as its text
        val textView = findViewById<TextView>(R.id.txtvGetMsg).apply {
            text = message
        }
    }
}