﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace WindowsFormsAppADB1
{
    class Aapt2Manager
    {
        public string adbCommand(string command)
        {
            try
            {

            ProcessStartInfo psInfo = new ProcessStartInfo();

            psInfo.FileName = @"aapt2.exe";                       //실행파일
            psInfo.Arguments = command;
            psInfo.UseShellExecute = false;                      //쉘 기능을 사용 하지 않는다.
            psInfo.CreateNoWindow = true;
            psInfo.RedirectStandardOutput = true;               //표준 출력을 리다이렉트
            psInfo.RedirectStandardError = true;               //표준 출력을 리다이렉트
            //psInfo.re
            Process p = Process.Start(psInfo);                  //어플리케이션 실행
            Console.WriteLine(command + " 수행중...");
            //p.WaitForExit();
            //p.wait();
            string output = p.StandardOutput.ReadToEnd();       //표준 출력 읽어 잡기
            System.Console.WriteLine(output);
            
            string outputError = p.StandardError.ReadToEnd(); // 에러 읽기 
            //output = output.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
            //txt_Output.Text = output;
            //string szTemp = output;
            //szTemp = szTemp.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
            System.Console.WriteLine(outputError);
            return outputError+"/"+output;

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return ex.ToString();
            }
        }
    }
}
