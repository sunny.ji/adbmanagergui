﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace WindowsFormsAppADB1
{
     class AdbManager
    {

        

        private void test2()
        {
                string output = adbCommand("shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest");
                //this.textBox1.Text = output;
        }

        public string adbCommand(string command)
        {

            ProcessStartInfo psInfo = new ProcessStartInfo();

            psInfo.FileName = @"adb.exe";                       //실행파일
            psInfo.Arguments = command;
            psInfo.UseShellExecute = false;                      //쉘 기능을 사용 하지 않는다.
            psInfo.CreateNoWindow = true;
            psInfo.RedirectStandardOutput = true;               //표준 출력을 리다이렉트
            psInfo.RedirectStandardError = true;               //표준 출력을 리다이렉트
            //psInfo.re
            Process p = Process.Start(psInfo);                  //어플리케이션 실행
            Console.WriteLine(command+" 수행중...");
            p.WaitForExit();

            string output = p.StandardOutput.ReadToEnd();       //표준 출력 읽어 잡기
            string outputError = p.StandardError.ReadToEnd();       //표준 출력 읽어 잡기

            //output = output.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
            //txt_Output.Text = output;

            //string szTemp = output;

            //szTemp = szTemp.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
            System.Console.WriteLine(outputError+"/"+output);
            if (outputError.Length > 1)
            {
                output= "ERROR: " + outputError + "/r/n"+ output;
            }
            return output;

        }

        //public void 
        public string getDevices()
        {
            return adbCommand("devices");

        }
        public string[] getDevicesStrings()
        {
            string result = adbCommand("devices");
            //char sp = @'\r\n';
            return Regex.Split(result, "\r\n"); 
        }

        public string runInstallGrant(String apkName)
        {
            //string cmd = " -s " + device + " shell install -g " + apkName;
            string cmd = "install -g " + apkName;
            Console.WriteLine(cmd);
            return adbCommand(cmd);
        }
        public string runInstallGrantTarget(string device, String apkName)
        {
            string cmd = " -s " + device + " install -g " + apkName;
           // string cmd = "install -g " + apkName;
            Console.WriteLine(cmd);
            return adbCommand(cmd);
        }
    }


}
