﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsAppADB1
{
    class FileUtil
    {
        static public void getFiles1(String path)
        {
            //string Path = @"C:\TEST";

            //해당 폴더가 존재하는지 확인

            if (System.IO.Directory.Exists(path))

            {

                //DirectoryInfo 객체 생성

                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);

                //해당 폴더에 있는 파일이름을 출력

                foreach (var item in di.GetFiles())

                {

                    Console.WriteLine(item.Name);

                }
            }
        }
    }
}