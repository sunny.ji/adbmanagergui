﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsAppADB1
{
    public partial class Form1 : Form
    {

        private String appFilesPath = @"./AppFiles";

        private const string V = "shell";

        public Form1()
        {
            InitializeComponent();
            textBox1.Text = "메롱메롱";
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "하잉하잉";
            Console.WriteLine("dddddddddddddddddddddddddd");

            test2();
            //test1();
        }


        private void test1()
        {
            System.Diagnostics.ProcessStartInfo proInfo = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.Process pro = new System.Diagnostics.Process();

            //proInfo.FileName = @"./adb shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest";
            proInfo.FileName = @"./adb";
            proInfo.CreateNoWindow = true;
            proInfo.UseShellExecute = false;

            proInfo.RedirectStandardOutput = true;
            proInfo.RedirectStandardInput = true;
            proInfo.RedirectStandardError = true;

            pro.StartInfo = proInfo;
            pro.Start();

            //pro.StandardInput.Write(@"adb shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);
            //pro.StandardInput.Write(@"shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);

            pro.StandardInput.Write(@"am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);
            pro.StandardInput.Close();

            string resultValue = pro.StandardOutput.ReadToEnd();
            pro.WaitForExit();
            pro.Close();
            Console.WriteLine(resultValue);
            this.textBox1.Text = resultValue;

        }

        private void test5()
        {
            System.Diagnostics.ProcessStartInfo proInfo = new System.Diagnostics.ProcessStartInfo();
            System.Diagnostics.Process pro = new System.Diagnostics.Process();

            //proInfo.FileName = @"./adb shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest";
            proInfo.FileName = @"./adb";
            proInfo.CreateNoWindow = true;
            proInfo.UseShellExecute = false;
            proInfo.Arguments = V;
            proInfo.RedirectStandardOutput = true;
            proInfo.RedirectStandardInput = true;
            proInfo.RedirectStandardError = true;

            pro.StartInfo = proInfo;
            pro.Start();

            //pro.StandardInput.Write(@"adb shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);
            //pro.StandardInput.Write(@"shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);

            pro.StandardInput.Write(@"start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest" + Environment.NewLine);
            //pro.StandardInput.Close();

            string resultValue = pro.StandardOutput.ReadToEnd();
            pro.WaitForExit();
            pro.Close();
            Console.WriteLine(resultValue);
            this.textBox1.Text = resultValue;

        }

        private void test2()
        {
            // 스크린 캡쳐 테스트
            //string[] szCom = new string[] {@"shell screencap -p /sdcard/Pictures/Screenshots/"+s+".png",
              //                          @"pull /sdcard/Pictures/Screenshots/"+s+".png "+output_path+@"\"+s+".png"};
            //foreach (string sz in szCom)
            {

                string output = getDeviceInfo("shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest");
                this.textBox1.Text = output;
            }

        }

        private static string getDeviceInfo(string command)
        {

            ProcessStartInfo psInfo = new ProcessStartInfo();

            psInfo.FileName = @"adb.exe";                       //실행파일
            psInfo.Arguments = command;
            psInfo.UseShellExecute = false;                      //쉘 기능을 사용 하지 않는다.
            psInfo.CreateNoWindow = true;
            psInfo.RedirectStandardOutput = true;               //표준 출력을 리다이렉트

            Process p = Process.Start(psInfo);                  //어플리케이션 실행
            p.WaitForExit();

            string output = p.StandardOutput.ReadToEnd();       //표준 출력 읽어 잡기

            //output = output.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
                                                                //txt_Output.Text = output;

            //string szTemp = output;

            //szTemp = szTemp.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정

            return output;

        }




        private void test3()
        {
            // 스크린 캡쳐 테스트
            //string[] szCom = new string[] {@"shell screencap -p /sdcard/Pictures/Screenshots/"+s+".png",
            //                          @"pull /sdcard/Pictures/Screenshots/"+s+".png "+output_path+@"\"+s+".png"};
            //foreach (string sz in szCom)
            {

                string output = getDeviceInfo3("shell am start -n com.example.mytestactivity/com.example.mytestactivity.MainActivity —es DEFINE_MSG tffffest");
                this.textBox1.Text = output;
            }

        }

        private static string getDeviceInfo3(string command)
        {

            ProcessStartInfo psInfo = new ProcessStartInfo();

            psInfo.FileName = @"adb.exe";                       //실행파일
            psInfo.Arguments = command;
            psInfo.UseShellExecute = false;                      //쉘 기능을 사용 하지 않는다.
            psInfo.CreateNoWindow = true;
            psInfo.RedirectStandardOutput = true;               //표준 출력을 리다이렉트

            Process p = Process.Start(psInfo);   //어플리케이션 실행
            
            p.WaitForExit();

            string output = p.StandardOutput.ReadToEnd();       //표준 출력 읽어 잡기

            //output = output.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정
            //txt_Output.Text = output;

            //string szTemp = output;

            //szTemp = szTemp.Replace("\r\r\n", "\n");            //줄바꿈 코드의 수정

            return output;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            FileUtil.getFiles1(appFilesPath);
        }
    }
}
