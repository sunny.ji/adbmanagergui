﻿
namespace WindowsFormsAppADB1
{
    partial class MyAapt2Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MyAapt2Form));
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_TargetApkName = new System.Windows.Forms.Label();
            this.btnCmdPackageName = new System.Windows.Forms.Button();
            this.btnCmdPermissions = new System.Windows.Forms.Button();
            this.btnCmdBadging = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Location = new System.Drawing.Point(1025, 679);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 52);
            this.button3.TabIndex = 22;
            this.button3.Text = "명령 전송";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(15, 679);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(985, 52);
            this.textBox2.TabIndex = 21;
            this.textBox2.Text = "dump packagename";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1095, 115);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(310, 474);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(15, 605);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 41);
            this.button1.TabIndex = 18;
            this.button1.Text = "앱 리스트 확인";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 41);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(1066, 548);
            this.textBox1.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(217, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "안드로이드 단말기 제어 솔루션";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1092, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(185, 15);
            this.label2.TabIndex = 23;
            this.label2.Text = "apk 목록 확인 [./AppFiles]";
            // 
            // label_TargetApkName
            // 
            this.label_TargetApkName.AutoSize = true;
            this.label_TargetApkName.BackColor = System.Drawing.Color.Red;
            this.label_TargetApkName.Font = new System.Drawing.Font("굴림", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_TargetApkName.Location = new System.Drawing.Point(1092, 76);
            this.label_TargetApkName.MinimumSize = new System.Drawing.Size(200, 15);
            this.label_TargetApkName.Name = "label_TargetApkName";
            this.label_TargetApkName.Size = new System.Drawing.Size(200, 30);
            this.label_TargetApkName.TabIndex = 24;
            // 
            // btnCmdPackageName
            // 
            this.btnCmdPackageName.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCmdPackageName.Location = new System.Drawing.Point(800, 605);
            this.btnCmdPackageName.Name = "btnCmdPackageName";
            this.btnCmdPackageName.Size = new System.Drawing.Size(174, 49);
            this.btnCmdPackageName.TabIndex = 25;
            this.btnCmdPackageName.Text = "packagename";
            this.btnCmdPackageName.UseVisualStyleBackColor = true;
            this.btnCmdPackageName.Click += new System.EventHandler(this.btnCmdPackageName_Click);
            // 
            // btnCmdPermissions
            // 
            this.btnCmdPermissions.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCmdPermissions.Location = new System.Drawing.Point(980, 605);
            this.btnCmdPermissions.Name = "btnCmdPermissions";
            this.btnCmdPermissions.Size = new System.Drawing.Size(162, 49);
            this.btnCmdPermissions.TabIndex = 26;
            this.btnCmdPermissions.Text = "permissions";
            this.btnCmdPermissions.UseVisualStyleBackColor = true;
            this.btnCmdPermissions.Click += new System.EventHandler(this.btnCmdPermissions_Click);
            // 
            // btnCmdBadging
            // 
            this.btnCmdBadging.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCmdBadging.Location = new System.Drawing.Point(1148, 605);
            this.btnCmdBadging.Name = "btnCmdBadging";
            this.btnCmdBadging.Size = new System.Drawing.Size(257, 49);
            this.btnCmdBadging.TabIndex = 27;
            this.btnCmdBadging.Text = "badging(매니페스트정보)";
            this.btnCmdBadging.UseVisualStyleBackColor = true;
            this.btnCmdBadging.Click += new System.EventHandler(this.btnCmdBadging_Click);
            // 
            // MyAapt2Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1417, 771);
            this.Controls.Add(this.btnCmdBadging);
            this.Controls.Add(this.btnCmdPermissions);
            this.Controls.Add(this.btnCmdPackageName);
            this.Controls.Add(this.label_TargetApkName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MyAapt2Form";
            this.Text = "AAPT 매니저";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_TargetApkName;
        private System.Windows.Forms.Button btnCmdPackageName;
        private System.Windows.Forms.Button btnCmdPermissions;
        private System.Windows.Forms.Button btnCmdBadging;
    }
}