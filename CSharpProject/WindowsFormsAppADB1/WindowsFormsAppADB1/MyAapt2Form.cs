﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsAppADB1.util;

namespace WindowsFormsAppADB1
{
    public partial class MyAapt2Form : Form
    {

        String FILE_APPS = @"./AppFiles";
        public MyAapt2Form()
        {
            InitializeComponent();
            button1_Click(null, null);


            this.label_TargetApkName.Size = new System.Drawing.Size(200, 30);
            this.label_TargetApkName.BackColor = Color.Red;
        }


        public void showLog(string msg)
        {
            //this.textBox1.Text += msg+ Environment.NewLine;

            LogWrite logWrite = new LogWrite();

            // 호출한 쓰레드가 작업쓰레드인가?
            if (textBox1.InvokeRequired)
            {
                // 작업쓰레드인 경우
                textBox1.BeginInvoke(new Action(() => this.textBox1.AppendText(msg)));
                logWrite.Write(msg);
            }
            else
            {
                this.textBox1.AppendText(msg);
                logWrite.Write(msg);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //명령 전송
            showLog("명령 전송");
            Aapt2Manager aapt2Manager = new Aapt2Manager();
            showLog(aapt2Manager.adbCommand(textBox2.Text));
            //showLog("고고싱");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //앱 리스트 확인
            showLog("앱 리스트 확인");
            try
            {
                FileUtil.getFiles1(FILE_APPS);
                createApkButton(FILE_APPS);
                //this.flowLayoutPanel1.Controls.Add(new Button());
            }
            catch (Exception ex)
            {
                LogWrite logWrite = new LogWrite();
                logWrite.Write(ex.Message);
                logWrite.Write(ex.StackTrace.ToString());
            }
        }

        private void createApkButton(String path)
        {
            //string Path = @"C:\TEST";

            //해당 폴더가 존재하는지 확인
            flowLayoutPanel1.Controls.Clear();

            if (System.IO.Directory.Exists(path))

            {

                //DirectoryInfo 객체 생성
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);

                //해당 폴더에 있는 파일이름을 출력
                foreach (var item in di.GetFiles())
                {
                    RadioButton btn = new RadioButton();
                    btn.Text = item.Name;
                    btn.AutoSize = true;
                    //btn.Size.Width="200"
                    flowLayoutPanel1.Controls.Add(btn);
                    btn.Click += new System.EventHandler(selectApkFile_Click);
                    Console.WriteLine(item.Name);
                }
            }
        }

        private void selectApkFile_Click(object sender, EventArgs e)
        {
            //앱 리스트 확인
            RadioButton btn = (RadioButton)sender;
            showLog("selectApkFile_Click "+ btn.Text);
            this.label_TargetApkName.Text = btn.Text;
            this.label_TargetApkName.BackColor = Color.Green;
            
           
        }

        private void btnCmdPackageName_Click(object sender, EventArgs e)
        {
            //패키지명 실행
            if (this.label_TargetApkName.Text.Length < 2)
            {
                showLog("apk를 선택하세요.");
                return;
            }

            String cmd = "dump packagename " + FILE_APPS + "/" + this.label_TargetApkName.Text;
            showLog(cmd + Environment.NewLine);

            Aapt2Manager aapt2Manager = new Aapt2Manager();
            showLog(aapt2Manager.adbCommand(cmd));

        }

        private void btnCmdPermissions_Click(object sender, EventArgs e)
        {
            //퍼미션 확인 실행
            if (this.label_TargetApkName.Text.Length < 2)
            {
                showLog("apk를 선택하세요.");
                return;
            }
            String cmd = "dump permissions " + FILE_APPS + "/" + this.label_TargetApkName.Text;
            showLog(cmd + Environment.NewLine);
            Aapt2Manager aapt2Manager = new Aapt2Manager();
            showLog(aapt2Manager.adbCommand(cmd));
        }

        private void btnCmdBadging_Click(object sender, EventArgs e)
        {
            //매니패스트 확인
            if (this.label_TargetApkName.Text.Length < 2)
            {
                showLog("apk를 선택하세요.");
                return;
            }
            String cmd = "dump badging " + FILE_APPS + "/" + this.label_TargetApkName.Text;
            showLog(cmd+ Environment.NewLine);
            Aapt2Manager aapt2Manager = new Aapt2Manager();
            showLog(aapt2Manager.adbCommand(cmd));
        }
    }
}
