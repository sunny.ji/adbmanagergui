﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using WindowsFormsAppADB1.util;

namespace WindowsFormsAppADB1
{
    public partial class MyAdbForm : Form
    {
        String FILE_APPS = @"./AppFiles";
        String deviceName = "";

        Dictionary<string, bool> devicesState = new Dictionary<string, bool>();//디바이스명, 사용 가능 유무 
        public MyAdbForm()
        {
            InitializeComponent();
            this.textBox1.Text = "로그를 출력합니다";
            this.textBox1.Select(textBox1.Text.Length, 0);
            this.textBox1.ScrollToCaret();
        }

        private void MyAdbForm_Load(object sender, EventArgs e)
        {
            button1_Click(null, null);
            button2_Click(null, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FileUtil.getFiles1(FILE_APPS);
                createApkButton(FILE_APPS);
                //this.flowLayoutPanel1.Controls.Add(new Button());
            }catch(Exception ex)
            {
                LogWrite logWrite = new LogWrite();
                logWrite.Write(ex.Message);
                logWrite.Write(ex.StackTrace.ToString());
            }


        }

        private void createApkButton(String path)
        {
            //string Path = @"C:\TEST";

            //해당 폴더가 존재하는지 확인
            flowLayoutPanel1.Controls.Clear();

            if (System.IO.Directory.Exists(path))

            {

                //DirectoryInfo 객체 생성
                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);

                //해당 폴더에 있는 파일이름을 출력
                foreach (var item in di.GetFiles())
                {
                    Button btn = new Button();
                    btn.Text = item.Name;
                    btn.AutoSize = true;
                    flowLayoutPanel1.Controls.Add(btn);
                    btn.Click += new System.EventHandler(buttonAppInstall);
                    //Console.WriteLine(item.Name);
                }
            }
        }

        private void buttonAppInstall1(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            showLog(btn.Text);
            AdbManager adbManager1 = new AdbManager();
            showLog(deviceName + "/" + FILE_APPS + "/" + btn.Text);
            int deviceCount = 0;
            foreach (string device in adbManager1.getDevicesStrings())
            {
                if (device.Length > 5)
                {
                    deviceCount++;
                }
            }
            showLog("설치 진행중...");
            if (deviceCount > 2)
            {
                showLog(adbManager1.runInstallGrantTarget(deviceName, FILE_APPS + "/" + btn.Text));
            }
            else
            {
                showLog(adbManager1.runInstallGrant(FILE_APPS + "/" + btn.Text));
            }

        }

        private void buttonAppInstall(object sender, EventArgs e)
        {
            try { 
                Button btn = (Button)sender;
                AdbManager adbManager1 = new AdbManager();
                showLog("설치 진행중...");
                if (this.devicesState.Count >= 2)
                {
                    //showLog(adbManager1.runInstallGrantTarget(deviceName, FILE_APPS + "/" + btn.Text));
                    Console.WriteLine("다 단말 제어 시도");
                    installMult(btn.Text);
                }
                else
                {
                    Console.WriteLine("단말 제어 시도");
                    showLog(adbManager1.runInstallGrant(FILE_APPS + "/" + btn.Text));
                }
            }
            catch (Exception ex)
            {
                LogWrite logWrite = new LogWrite();
                logWrite.Write(ex.Message);
                logWrite.Write(ex.StackTrace.ToString());
            }

        }
        
        private void rdoBtnDevceName(object sender, EventArgs e)
        {
            RadioButton rdoBtn = (RadioButton)sender;
            showLog(rdoBtn.Text);
            deviceName = rdoBtn.Text;
        }
        private void button2_Click(object sender, EventArgs e)
        {

            AdbManager adbManager1 = new AdbManager();

            string devices = adbManager1.getDevices();
            showLog(devices);
            
            string[] devicesList = System.Text.RegularExpressions.Regex.Split(devices, "\r\n");
            this.flowLayoutPanel2.Controls.Clear();
            this.devicesState.Clear();

            for (int i=1; i<devicesList.Length; i++)
            {
             
                if(devicesList[i].Length>5)
                {
                    showLog(devicesList[i]);
                    string deviceName=devicesList[i].Split('\t')[0];
                    string deviceActivationState = devicesList[i].Split('\t')[1];

                    RadioButton btn = new RadioButton();
                    btn.AutoSize = true;
                    //btn.Name = "radioButton2";
                    btn.Size = new System.Drawing.Size(200, 30);
                    btn.TabIndex = 15;
                    btn.TabStop = true;
                    //btn.Text = devicesList[i];
                    btn.Text = deviceName;
                    btn.UseVisualStyleBackColor = true;
                    //unauthorized 연결 안됨
                    //device 연결 됨 
                    btn.Font=new Font(btn.Font,FontStyle.Bold);
                    
                    if (deviceActivationState.Equals("device"))
                    {
                        btn.BackColor = Color.Green;
                    }
                    else if(deviceActivationState.Equals("unauthorized"))
                    {
                        btn.BackColor = Color.Red;
                    }
                    else
                    {
                        btn.BackColor = Color.Yellow;
                    }

                    if (deviceActivationState.Equals("device")) {
                        devicesState.Add(deviceName, true);
                     }
                    else
                    {
                        devicesState.Add(deviceName, false);
                    }

                    btn.Click+= new System.EventHandler(rdoBtnDevceName);
                    this.flowLayoutPanel2.Controls.Add(btn);
                }
            }
        }

        public string str2hex(string strData)
        {
            string resultHex = string.Empty;
            byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

            foreach (byte byteStr in arr_byteStr)
                resultHex += string.Format("{0:X2}", byteStr);

            return resultHex;
        }
        public void showLog(string msg)
        {
            //this.textBox1.Text += msg+ Environment.NewLine;

            LogWrite logWrite = new LogWrite();

            // 호출한 쓰레드가 작업쓰레드인가?
            if (textBox1.InvokeRequired)
            {
                // 작업쓰레드인 경우
                textBox1.BeginInvoke(new Action(() => this.textBox1.AppendText(msg)));
                logWrite.Write(msg);
            }
            else
            {
                this.textBox1.AppendText(msg);
                logWrite.Write(msg);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            AdbManager adb = new AdbManager();
            String str = adb.adbCommand(this.textBox2.Text);
            showLog(str);
        }

        private void installMult(string fileName)
        {
            
            foreach (KeyValuePair<string, bool> kvp in this.devicesState)
            {
                Console.WriteLine("Key: " + kvp.Key);
                Console.WriteLine("Value: " + kvp.Value);

                Thread myThread = new Thread(() => installDevice(kvp.Key, fileName));
                if (kvp.Value) { 
                    myThread.Start();
                }
            }
            //Thread myThread = new Thread(() => installDevice(device, fileName));
            //myThread.Start();
        }

        private void installDevice(String deviceName, String filePathName)
        {
            AdbManager adbManager = new AdbManager();
            showLog(deviceName+" : 시도중" );
            showLog(deviceName+" : "+adbManager.runInstallGrantTarget(deviceName, FILE_APPS + "/" + filePathName));
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MyAapt2Form form = new MyAapt2Form();
            form.Show();
        }
    }
}
