# 작성자 정보
https://helloit.tistory.com

# AdbManagerGUI

안드로이드 디바이스에 앱을 설치해 주는 어플리케이션 구현.

## 프로젝트의 목적:
- 안드로이드 디바이스를 제어하는 도구를 GUI로 구현
- 윈도우 향 프로그램을 개발하며, C# 및 Java의 언어를 이용하여 구현한다.

## 프로젝트의 범위:
- android tools에서 필요한 adb, aapt의 기능을 이용하여 윈도우 화면으로 확인한다.

## 진행 단계:
- 개발 환경 구축. (완료)
- C#, Java, Android 프로젝트 디렉토리 생성. (완료)
- C#을 이용한 adb 설치, command code 전송 Gui 데모 (완료)
- Java Gui를 이용한 adb 제어 기능 데모 (준비)
- android tools의 aapt 제어 기능 데모 (준비)

## 결과물
- C#을 이용한 단말기 제어 shell 접근 제어 기능(결과물)
   (단말기 선택, 앱 삭제 설치가 가능하다.)
>>><img src="./etc/output1.PNG" width="60%">


